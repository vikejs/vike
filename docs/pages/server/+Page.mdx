import { Link, Warning, NoteWithCustomIcon } from '@brillout/docpress'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import '../../components/tabs.css'

<NoteWithCustomIcon icon={<span style={{fontSize: "1.1em"}}>⚗️</span>}>`vike-server` will be released very shortly (ETA this week). In the meantime, see <Link href="/integration#server-manual-integration" doNotInferSectionTitle /> instead.</NoteWithCustomIcon>

We recommend using the Vike extension `vike-server` which comes with many benefits such as:
- Out-of-the-box support for popular servers (Express, Hono, Hattip, Elysia, etc.)
- Out-of-the-box support for popular deployments (Cloudflare, Vercel, Netlify, etc.)
- HMR

Version history: [`CHANGELOG.md`](https://github.com/vikejs/vike-server/blob/main/packages/vike-server/CHANGELOG.md)  
Examples: [Bati](https://batijs.dev) and [`examples/`](https://github.com/vikejs/vike-server/tree/main/examples)  
Source code: [GitHub > `vikejs/vike-server`](https://github.com/vikejs/vike-server)  

The documentation of `vike-server` is on this page.

Getting started: <Link href="#add-vike-server-to-an-existing-vike-app" />.

> Alternatively, instead of using a server, you can <Link href="/pre-rendering">pre-render</Link> your pages and deploy to a <Link href="/static-hosts">static host</Link>.

> If you want more control over server integration, see <Link href="/integration#server-manual-integration" doNotInferSectionTitle /> instead.


## Add `vike-server` to an existing Vike app

To add `vike-server` to an existing Vike app: install the `vike-server` npm package (e.g. `$ npm install vike-server`) then extend your existing <Link href="/config#files">+config.js</Link> file (or create one) with `vike-server`:
```js
// +config.js
import vikeServer from 'vike-server/config' // [!code ++]

export const config = {
  extends: [vikeServer],  // [!code ++]
  // Points to your server entry
  server: 'server/index.js'  // [!code ++]
}
```

Update your production `script` in `package.json`:
```json
// package.json

"scripts": {
  "dev": "vike dev",
  "build": "vike build",
  "prod": "NODE_ENV=production node dist/server/index.js" // [!code ++]
}
```

Create (or update) your server entry:

<Tabs>
  <TabList>
    <Tab>Express.js</Tab>
    <Tab>Hono</Tab>
    <Tab>Fastify</Tab>
    <Tab>H3</Tab>
    <Tab>Elysia</Tab>
  </TabList>

  <TabPanel>
    ```js
    // server/index.js

    import express from 'express'
    // apply() installs Vike's middlewares onto the server
    import { apply } from 'vike-server/express'
    // serve() allows a unique file to target Node.js, Cloudflare, Vercel, Deno, Bun, etc...
    import { serve } from 'vike-server/express/serve'

    function startServer() {
      const app = express()
      apply(app)
      const port = 3000
      // port is only used when running on non-serverless environments
      return serve(app, { port })
    }

    export default startServer()
    ```
  </TabPanel>
  <TabPanel>
    ```js
    // server/index.js

    import { Hono } from 'hono'
    // apply() installs Vike's middlewares onto the server
    import { apply } from 'vike-server/hono'
    // serve() allows a unique file to target Node.js, Cloudflare, Vercel, Deno, Bun, etc...
    import { serve } from 'vike-server/hono/serve'

    function startServer() {
      const app = new Hono()
      apply(app)
      const port = 3000
      // port is only used when running on non-serverless environments
      return serve(app, { port })
    }

    export default startServer()
    ```
  </TabPanel>
  <TabPanel>
    ```js
    // server/index.js

    import fastify from 'fastify'
    import rawBody from 'fastify-raw-body'
    // apply() installs Vike's middlewares onto the server
    import { apply } from 'vike-server/fastify'
    // serve() allows a unique file to target Node.js, Cloudflare, Vercel, Deno, Bun, etc...
    import { serve } from 'vike-server/fastify/serve'

    async function startServer() {
      const app = fastify({
        // /!\ Mandatory for HMR support
        forceCloseConnections: true
      })

      // /!\ Mandatory for Vike middlewares to operate properly
      await app.register(rawBody)
      await apply(app)
      const port = 3000
      // port is only used when running on non-serverless environments
      return serve(app, { port })
    }

    export default startServer()
    ```
  </TabPanel>
  <TabPanel>
    ```js
    // server/index.js

    import { createApp } from 'h3'
    // apply() installs Vike's middlewares onto the server
    import { apply } from 'vike-server/h3'
    // serve() allows a unique file to target Node.js, Cloudflare, Vercel, Deno, Bun, etc...
    import { serve } from 'vike-server/h3/serve'

    function startServer() {
      const app = createApp()
      apply(app)
      const port = 3000
      // port is only used when running on non-serverless environments
      return serve(app, { port })
    }

    export default startServer()
    ```
  </TabPanel>
  <TabPanel>
    ```js
    // server/index.js

    import { Elysia } from 'elysia'
    // apply() installs Vike's middlewares onto the server
    import { apply } from 'vike-server/elysia'
    // serve() allows a unique file to target Node.js, Cloudflare, Vercel, Deno, Bun, etc...
    import { serve } from 'vike-server/elysia/serve'

    function startServer() {
      const app = apply(new Elysia())
      const port = 3000
      // port is only used when running on non-serverless environments
      return serve(app, { port })
    }

    export default startServer()
    ```
  </TabPanel>
</Tabs>


## Deployment

In order to deploy, publish `dist/` to production and execute `dist/server/index.js`.

> We're currently working on out-of-the-box support for Cloudflare and Vercel (ETA the next couple of weeks). In the meantime, see:
> - <Link href="/vercel" />
> - <Link href="/cloudflare-pages" />
> - <Link href="/cloudflare-workers" />


## Custom `pageContext`

To define <Link href="/pageContext#custom">custom `pageContext`</Link> properties:

```ts
apply(app, {
  pageContext(runtime) {
    return {
      user: runtime.req.user
    }
  }
})
```

> The `runtime` object is a [`RuntimeAdapter`](https://universal-middleware.dev/reference/runtime-adapter) (`vike-server` uses [universal-middleware](https://universal-middleware.dev/) under the hood).

> The `runtime` object is also available at `pageContext.runtime`. Consequently, you don't necessarily need to use the `pageContext` function above
> in order to retrieve information, such as `pageContext.runtime.req.user`, in Vike hooks and UI components (see <Link href="/usePageContext" />).

<br/>

## Standalone

With `standalone: true`, the build output directory ([`dist/`](https://vite.dev/config/build-options.html#build-outdir)) contains everything needed for deployment. This means that, in production, only the `dist/` directory is required (you can remove `node_modules/` and skip `$ npm install`).

<Warning>
 This feature is experimental and may break upon any version update.
</Warning>

>  If the production code built with `standalone: true` fails to run with errors like `ENOENT: no such file or directory`, then disable standalone mode or replace
>  the npm package throwing the error with another npm package. (The issue is that some npm package have dependencies that aren't explicit and, consequently, cannot be statically analyzed to be included in the bundle.)

```js
// +config.js

import vikeServer from 'vike-server/config'

export const config = {
  // ...
  extends: [vikeServer],
  server: {
    entry: 'server/index.js',
    standalone: true
  }
}
```

Options:

```js
export const config = {
  // ...
  extends: [vikeServer],
  server: {
    entry: 'server/index.js',
    standalone: {
      esbuild: {
        minify: true,
        // ... or any other esbuild option
      }
    }
  }
}
```

> Instead of using `standalone: true`, we recommend tools such as [`pnpm deploy --prod`](https://pnpm.io/cli/deploy).
> This provides better control over packed files and ensures greater compatibility.


## Compression

In production, `vike-server` compresses all Vike responses.

You can disable it:

```js
apply(app, {
  compress: false
})
```


## HTTPS

In production, Vike is just a server middleware — there is nothing to take into consideration.

If you want to use HTTPS in development as well, then make sure to [pass the HTTPS certificates to Vite's dev server](https://vitejs.dev/config/server-options.html#server-https).


## See also

- <Link href="/integration#server-manual-integration" doNotInferSectionTitle />
- <Link href="/integration#non-javascript-backend" doNotInferSectionTitle />
- <Link href="/renderPage" />
